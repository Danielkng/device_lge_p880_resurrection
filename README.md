# Resurrection Remix P880 - Lollipop 5.1
Fell free to fork it or use it as base, but please ask me before!
Credits go P880-Dev team for letting me use their device tree as base and to laufersteppenwolf, Adam77Root and lj50036 
on xda for helping me to fix existing errors!

# Local Manifest

Here you can find my local_manifest: http://pastebin.com/giGdz09j

# Vendor Files

Also download the Vendor Files from here https://github.com/DanielKng/proprietary_vendor_lge_p880 and replace them with them in /vendor/lge.


